﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace ListExportImportApp
{
    public partial class MainForm : Form
    {
        private List<string> items = new List<string>();

        public MainForm()
        {
            InitializeComponent();
            itemList.View = View.Details;
            itemList.Columns.Add("Items", -2, HorizontalAlignment.Left);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            string newItem = itemTextBox.Text.Trim();
            if (!string.IsNullOrEmpty(newItem)) 
            {
                items.Add(newItem);
                UpdateListView();
                itemTextBox.Clear();
            }
        }
        private void UpdateListView()
        {
            itemList.Items.Clear();
            foreach (string item in items)
            {
                itemList.Items.Add(item);
            }
        }



        private void LoadDataFromFile(string filePath)
        {
            try
            {
                items.Clear();
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        items.Add(line);
                    }
                }
                UpdateListView();
            }
            catch (Exception ex)
            {
                LogException(ex);
                MessageBox.Show("Error importing data. Please see log file for details.");
            }
        }


        private void LogException(Exception ex)
        {
            string logFilePath = "error_log.txt";
            try
            {
                using (StreamWriter writer = new StreamWriter(logFilePath, true))
                {
                    writer.WriteLine($"[{DateTime.Now}] {ex.GetType().FullName}: {ex.Message}");
                }
            }
            catch
            {
                // Se ocorrer uma exceção ao registrar a exceção, ignore silenciosamente
            }
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = saveFileDialog.FileName;
                try
                {
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        foreach (string item in items)
                        {
                            writer.WriteLine(item);
                        }
                    }
                    MessageBox.Show("Data exported successfully!");
                }
                catch (Exception ex)
                {
                    LogException(ex);
                    MessageBox.Show("Error exporting data. Please see log file for details.");
                }
            }

        }

        private void importButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text Files (*.txt)|*.txt";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                LoadDataFromFile(filePath);
            }
        }
    }
}
